package pl.edu.agh.ztis;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;

/**
 * Created by Kacper on 2015-05-31.
 */
public class TransactionHelper {
    private final int limit;
    private int counter;
    private final GraphDatabaseService graphDatabaseService;
    private Transaction activeTx;

    public TransactionHelper(int limit, GraphDatabaseService graphDatabaseService) {
        this.limit = limit;
        this.graphDatabaseService = graphDatabaseService;
    }

    public void beginTx() {
        if (this.activeTx == null)
            this.activeTx = this.graphDatabaseService.beginTx();
    }

    public void commitTx() {
        this.activeTx.success();
    }

    public void closeTx() {
        this.activeTx.close();
        this.activeTx = null;
        this.counter = 0;
    }

    public void commitAndCloseTx() {
        this.counter++;
        if (counter > limit) {
            System.out.println("restart");
            this.commitTx();
            this.closeTx();
        }
    }

    public void resetTx() {
        this.commitAndCloseTx();
        this.beginTx();
    }
    public void increaseCounter() {
        this.counter++;
    }
}
