package pl.edu.agh.ztis.transformer;

import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.schema.IndexDefinition;
import org.neo4j.graphdb.schema.Schema;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.tooling.GlobalGraphOperations;
import pl.edu.agh.ztis.TransactionHelper;

import javax.xml.bind.DatatypeConverter;
import java.util.Calendar;
import java.util.Iterator;

import static org.neo4j.graphdb.DynamicRelationshipType.withName;

/**
 * Created by Kacper on 2015-04-26.
 */
public class TypeRemover {
    private GraphDatabaseService graphDb;
    //private static final RelationshipType rdfType = withName("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");

    public static void main(String[] args) {
        TypeRemover typeRemover = new TypeRemover();
        typeRemover.createDb(args[0]);
        typeRemover.setTypeProperty();
        typeRemover.deleteTypeNodes();
        typeRemover.closeDb();
    }

    private void closeDb() {
        graphDb.shutdown();
    }

    private void createDb(String dbDir) {
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(dbDir);
        registerShutdownHook(graphDb);
    }

    private static void registerShutdownHook(final GraphDatabaseService graphDb) {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    public void setTypeProperty() {
        Label myLabel = DynamicLabel.label("Person");
        IndexDefinition indexDefinition;
        try ( Transaction tx = graphDb.beginTx() )
        {
            Schema schema = graphDb.schema();
            indexDefinition = schema.indexFor(myLabel )
                    .on( "century" )
                    .create();
            tx.success();
        }



        int counter = 0;
        int nodeCounter = 0;
        TransactionHelper transactionHelper = new TransactionHelper(30000, graphDb);
        transactionHelper.beginTx();
        System.out.println("Starting");
        for (Node node : GlobalGraphOperations.at(graphDb).getAllNodes()) {
            for (Path position : graphDb.traversalDescription()
                    .depthFirst()
                    .relationships(withName("http://dbpedia.org/ontology/birthDate"), Direction.OUTGOING)
                    .relationships(withName("http://dbpedia.org/ontology/birthYear"), Direction.OUTGOING)
                    .evaluator(Evaluators.fromDepth(1))
                    .evaluator(Evaluators.toDepth(1))
                    .traverse(node)) {
                Node startNode = position.startNode();
                startNode.setProperty("century",getCentury(startNode));

                startNode.addLabel(myLabel);
                Node endNode = position.endNode();
                counter++;
                transactionHelper.resetTx();
            }
            nodeCounter++;
            if (nodeCounter > 10000) {
                nodeCounter = 0;
                System.out.println("0.1% nodes processed");
            }
        }
        transactionHelper.commitTx();
        transactionHelper.closeTx();
        //tx.success();
        System.out.println(counter);
    }

    private int getCentury(Node startNode) {
        int minCentury=22;
        for (Relationship rel:startNode.getRelationships(Direction.OUTGOING)){

            if(rel.getType().name().equals("http://dbpedia.org/ontology/birthDate")){
                Node otherNode = rel.getOtherNode(startNode);

                Calendar cal = DatatypeConverter.parseDateTime(otherNode.getProperty("value").toString());
                int century = cal.get(Calendar.YEAR );
                if(century>=0){
                    century=century/100+1;
                }else{
                    century=century/100-1;
                }
                return century;
            }  else if(rel.getType().name().equals("http://dbpedia.org/ontology/birthYear")){
                Node otherNode = rel.getOtherNode(startNode);

                Calendar cal = DatatypeConverter.parseDateTime(otherNode.getProperty("value").toString());
                int century = cal.get(Calendar.YEAR )/100+1;
                if(cal.get(Calendar.ERA)==0){
                    century=-century;
                }
                if(century<minCentury){
                    minCentury=century;
                }
            }
        }
        return minCentury;
    }


    public void deleteTypeNodes() {
        try (Transaction tx = graphDb.beginTx()) {
            tx.success();
        }
    }
}
