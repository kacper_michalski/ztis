package pl.edu.agh.ztis.transformer;

import org.neo4j.graphdb.RelationshipType;

import static org.neo4j.graphdb.DynamicRelationshipType.withName;

/**
 * Created by Kacper on 2015-04-27.
 */
public interface RelTypes {
    public static final RelationshipType rdfType = withName("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
    public static final RelationshipType schemaPerson = withName("http://schema.org/Person");
    public static final RelationshipType connected = withName("connected");
    public static final RelationshipType connected2 = withName("connected2");
    public static final String foafPerson = "http://xmlns.com/foaf/0.1/Person";
    public static final RelationshipType connected3 = withName("connected3");
    public static final RelationshipType connected4 = withName("connected4");
    public static final RelationshipType connected5 = withName("connected5");
    public static final RelationshipType connected6 = withName("connected6");
    public static final RelationshipType connected7 = withName("connected7");
    public static final RelationshipType connected8 = withName("connected8");
}
