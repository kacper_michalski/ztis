package pl.edu.agh.ztis.transformer;

import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.tooling.GlobalGraphOperations;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Kacper on 2015-05-24.
 */
public class EdgeFilter {
    private GraphDatabaseService graphDb;
    private Set<RelationshipType> allowedRelationshipsSet;
    private Set<RelationshipType> disallowedRelationshipsSet;

    public static void main(String[] args) {
        EdgeFilter edgeFilter = new EdgeFilter();
        edgeFilter.createDb();
        edgeFilter.discoverConnections();
        edgeFilter.closeDb();
    }

    private void createDb() {
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase("sampleDB");
        registerShutdownHook(graphDb);
        allowedRelationshipsSet = new HashSet<RelationshipType>();
        disallowedRelationshipsSet = new HashSet<RelationshipType>();
        //todo init
    }

    private static void registerShutdownHook(final GraphDatabaseService graphDb) {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    private void closeDb() {
        graphDb.shutdown();
    }

    public void discoverConnections() {

        try (Transaction tx = graphDb.beginTx()) {
            Relationship toDelete = null;

            for (Relationship rel : GlobalGraphOperations.at(graphDb).getAllRelationships()) {

                try (Transaction internalTx = graphDb.beginTx()) {
                    if (toDelete != null)
                        toDelete.delete();
                    if (!allowedRelationshipsSet.contains(rel.getType()) || disallowedRelationshipsSet.contains(rel.getType())) {
                        toDelete = rel;
                    }
                    internalTx.success();
                }

            }
            tx.success();
        }
    }

    private void addWeightedConnection(Path position, Node startNode, Node endNode) {
        if (startNode.hasRelationship(RelTypes.connected)) {
            for (Relationship rel : startNode.getRelationships(RelTypes.connected)) {
                if (rel.getEndNode().equals(endNode)) {
                    int length = position.length();
                    Double weight = (Double) rel.getProperty("weight");
                    rel.setProperty("weight", weight + 1.0 / (double) length);
                    return;
                }
            }
        }
        Relationship rel = startNode.createRelationshipTo(endNode, RelTypes.connected);
        int length = position.length();
        rel.setProperty("weight", 1.0 / (double) length);
    }
}
