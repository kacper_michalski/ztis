package pl.edu.agh.ztis.transformer;

import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.Uniqueness;
import org.neo4j.tooling.GlobalGraphOperations;
import pl.edu.agh.ztis.TransactionHelper;

/**
 * Created by Kacper on 2015-04-27.
 */
public class NonPersonRemover {
    private GraphDatabaseService graphDb;

    public static void main(String[] args) {
        NonPersonRemover nonPersonRemover = new NonPersonRemover();
        nonPersonRemover.createDb(args[1]);
        nonPersonRemover.discoverConnections(Integer.parseInt(args[0]));
        nonPersonRemover.closeDb();
    }

    private void createDb(String dbDir) {
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(dbDir);
        registerShutdownHook(graphDb);
    }

    private static void registerShutdownHook(final GraphDatabaseService graphDb) {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    private void closeDb() {
        graphDb.shutdown();
    }
    public void discoverConnections(int century) {

        TransactionHelper transactionHelper = new TransactionHelper(10000, graphDb);
        transactionHelper.beginTx();
        long counter = 0;
        long extCounter = 0;
        Label myLabel = DynamicLabel.label("Person");
            for (Node node : graphDb.findNodesByLabelAndProperty(myLabel,"century",century)) {

                for (Path position : graphDb.traversalDescription()
                        .depthFirst()
                        .uniqueness(Uniqueness.NODE_GLOBAL)
                        .evaluator(Evaluators.fromDepth(1))
                        .evaluator(Evaluators.toDepth(2))
                        .evaluator(new PersonEvaluator())
                        .expand(new PersonPathExpander())
                        .traverse(node)) {
                    Node startNode = position.startNode();
                    Node endNode = position.endNode();
                    if (endNode.hasProperty("century") && endNode.getProperty("century").equals(17))
                        addWeightedConnection(position, startNode, endNode);
                        transactionHelper.increaseCounter();
                        counter++;

                }
                transactionHelper.resetTx();

                //}
                if (counter - 30000 > 0) {
                    System.out.println(++extCounter);
                    counter = 0;
                }

            }
        transactionHelper.commitTx();
        transactionHelper.closeTx();
    }

    private void addWeightedConnection(Path position, Node startNode, Node endNode) {
        //System.out.println("adding");
        if (startNode.hasRelationship(RelTypes.connected8)) {
            for (Relationship rel : startNode.getRelationships(RelTypes.connected8)) {
                if (rel.getEndNode().equals(endNode)) {
                    int length = position.length();
                    Double weight = (Double) rel.getProperty("weight");
                    rel.setProperty("weight", weight + 1.0 / (double) length);
                    return;
                }
            }
        }
        Relationship rel = startNode.createRelationshipTo(endNode, RelTypes.connected8);
        int length = position.length();
        rel.setProperty("weight", 1.0 / (double) length);
    }
}
