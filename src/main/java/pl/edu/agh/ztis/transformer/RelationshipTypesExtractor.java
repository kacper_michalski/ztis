package pl.edu.agh.ztis.transformer;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.tooling.GlobalGraphOperations;

import java.util.Set;

/**
 * Created by Kacper on 2015-05-25.
 */
public class RelationshipTypesExtractor {
    private GraphDatabaseService graphDb;
    private Set<RelationshipType> allowedRelationshipsSet;
    private Set<RelationshipType> disallowedRelationshipsSet;

    public static void main(String[] args) {
        RelationshipTypesExtractor relationshipTypesExtractor = new RelationshipTypesExtractor();
        relationshipTypesExtractor.createDb();
        relationshipTypesExtractor.discoverConnections();
        relationshipTypesExtractor.closeDb();
    }

    private void createDb() {
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase("integracjaDB2");
        registerShutdownHook(graphDb);
    }

    private static void registerShutdownHook(final GraphDatabaseService graphDb) {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    private void closeDb() {
        graphDb.shutdown();
    }

    public void discoverConnections() {

        try (Transaction tx = graphDb.beginTx()) {

            for (RelationshipType rel : GlobalGraphOperations.at(graphDb).getAllRelationshipTypes()) {

                System.out.println(rel.name());

            }
            tx.success();
        }
    }

}
