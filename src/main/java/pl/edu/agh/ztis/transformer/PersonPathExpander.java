package pl.edu.agh.ztis.transformer;

import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.PathExpander;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.traversal.BranchState;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Kacper on 2015-05-03.
 */
public class PersonPathExpander implements PathExpander {
    private Set<String> allowedRelTypesSet = new HashSet<String>();

    public PersonPathExpander() {
        allowedRelTypesSet.add("http://dbpedia.org/ontology/previousWork");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/subsequentWork");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/editing");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/starring");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/series");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/firstPublicationDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/lastPublicationDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/licensee");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/foundingYear");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/recordDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/foundingDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/album");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/guest");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/team");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/publicationDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/formationDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/hometown");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/absoluteMagnitude");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/discovered");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/locationCity");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/keyPerson");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/completionDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/related");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/leaderFunction");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/membership");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/origin"); //?????
        allowedRelTypesSet.add("http://dbpedia.org/ontology/maintainedBy");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/title");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/isPartOf");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/locatedInArea");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/role");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/birthDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/usedInWar");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/characterInPlay");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/premiereDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/successor");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/disbanded");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/activeYearsEndDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/activeYearsStartDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/battle");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/secondCommander");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/alias");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/commander");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/foundedBy");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/basedOn");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/distributingCompany");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/distributingLabel");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/owningOrganisation");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/battleHonours");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/openingDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/date");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/isPartOfMilitaryConflict");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/extinctionYear");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/secondLeader");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/firstLeader");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/leader");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/foundationPlace");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/owningCompany");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/musicBy");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/endDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/startDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/deathPlace");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/openingFilm");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/electionDateLeader");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/politicalPartyOfLeader");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/thirdCommander");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/extinctionDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/meanRadius");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/politicalPartyInLegislature");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/leaderParty");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/deathDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/buildingEndDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/head");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/settingOfPlay");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/fourthCommander");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/governingBody");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/museum");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/currentMember");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/predecessor");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/previousEditor");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/closingDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/latestReleaseDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/latestReleaseVersion");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/operatedBy");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/subjectOfPlay");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/parent");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/child");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/spouse");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/firstAppearance");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/knownFor");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/influencedBy");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/notableWork");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/lastAppearance");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/formerPartner");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/plays");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/formerCoach");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/currentPartner");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/monarch");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/formerTeam");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/debutTeam");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/opponent");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/currentlyUsedFor");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/honours");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/mission");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/dateOfBurial");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/heir");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/foundingPerson");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/rival");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/precursor");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/canonizedDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/canonizedBy");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/beatifiedBy");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/hallOfFame");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/chorusCharacterInPlay");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/firstWin");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/dateConstruction");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/lastWin");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/commissioningDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/beatifiedDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/orderDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/introductionDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/fundedBy");
        allowedRelTypesSet.add("http://xmlns.com/foaf/0.1/familyName");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/rebuildDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/argueDate");
        allowedRelTypesSet.add("http://dbpedia.org/ontology/decideDate");
    }

    @Override
    public Iterable<Relationship> expand(Path path, BranchState branchState) {
        List<Relationship> results = new ArrayList<Relationship>();
        for (Relationship r : path.endNode().getRelationships()) {
            //System.out.println(r.getType().name());
            if (allowedRelTypesSet.contains(r.getType().name())) {
                //System.out.println("path");
                results.add(r);
            }
        }
        return results;
    }

    @Override
    public PathExpander reverse() {
        return null;
    }
}
