package pl.edu.agh.ztis.transformer;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.traversal.Evaluation;
import org.neo4j.graphdb.traversal.Evaluator;

/**
 * Created by Kacper on 2015-04-27.
 */
public class PersonEvaluator implements Evaluator {
    @Override
    public Evaluation evaluate(Path path) {
        Node node = path.startNode();
        //System.out.println(path.length());
        //System.out.println("from " + node.toString() + "to" + path.endNode().toString());
        if (!node.hasProperty("century") || !node.getProperty("century").equals(17)) {
            //System.out.println("EXCLUDE_AND_PRUNE");
            return Evaluation.EXCLUDE_AND_PRUNE;
        }
        node = path.endNode();
        if (path.length() > 0 && node.hasProperty("century") && node.getProperty("century").equals(17)) {
            //System.out.println("INCLUDE_AND_PRUNE");
            return Evaluation.INCLUDE_AND_PRUNE;
        } else {
            //System.out.println("INCLUDE_AND_CONTINUE");
            return Evaluation.INCLUDE_AND_CONTINUE;
        }
    }
}
