package pl.edu.agh.ztis.transformer;

import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.tooling.GlobalGraphOperations;

import javax.xml.bind.DatatypeConverter;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Kacper on 2015-05-31.
 */
public class Test {
    private GraphDatabaseService graphDb;
    //private static final RelationshipType rdfType = withName("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");

    public static void main(String[] args) {
        Test test = new Test();
        test.createDb();
        test.test();
        test.closeDb();
    }

    private void closeDb() {
        graphDb.shutdown();
    }

    private void createDb() {
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase("integracjaDB2");
        registerShutdownHook(graphDb);
    }

    private static void registerShutdownHook(final GraphDatabaseService graphDb) {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    public void test() {
        int counter = 0;
        long nodeCounter=0;
        long relCounter=0;
        try (Transaction tx = graphDb.beginTx()) { /*
            Set<String> relTypes = new HashSet<String>();
            for (Node node : GlobalGraphOperations.at(graphDb).getAllNodes()) {
                nodeCounter++;
                if(node.hasProperty("value") ){
                    /*if(node.getProperty("value").equals(RelTypes.foafPerson)) {
                        System.out.println("found");
                        for (Relationship rel:node.getRelationships(Direction.INCOMING)){
                            counter++;
                        }
                    }else if(node.getProperty("value").toString().contains("person")){
                        relTypes.add(node.getProperty("value").toString());
                    } */ /*
                    if(node.getProperty("value").equals("http://dbpedia.org/resource/Aristotle")) {
                        //Miki_Higashino
                        System.out.println("labels");
                        for(Label label:node.getLabels()){
                            System.out.println(label);
                        }

                        System.out.println("labels");
                        System.out.println("found");
                        for (Relationship rel:node.getRelationships(Direction.OUTGOING)){
                            relTypes.add(rel.getType().name());
                            counter++;
                            if(rel.getType().name().equals("http://dbpedia.org/ontology/birthDate")){
                                Node otherNode = rel.getOtherNode(node);
                                for(String prop:otherNode.getPropertyKeys()) {
                                    System.out.println(prop.toString()+" "+otherNode.getProperty(prop).toString());
                                }
                                Calendar cal = DatatypeConverter.parseDateTime(otherNode.getProperty("value").toString());
                                int century = cal.get(Calendar.YEAR );
                                if(century>=0){
                                    century=century/100+1;
                                }else{
                                    century=century/100-1;
                                }
                                System.out.println("century1 "+century);
                            }
                            if(rel.getType().name().equals("http://dbpedia.org/ontology/birthYear")){
                                Node otherNode = rel.getOtherNode(node);
                                for(String prop:otherNode.getPropertyKeys()) {
                                    System.out.println(prop.toString()+" "+otherNode.getProperty(prop).toString());
                                }
                                Calendar cal = DatatypeConverter.parseDateTime(otherNode.getProperty("value").toString());
                                int century = cal.get(Calendar.YEAR )/100+1;
                                if(cal.get(Calendar.ERA)==0){
                                    century=-century;
                                }
                                System.out.println("century2 "+century);
                            }
                        }
                        for(String prop:node.getPropertyKeys()) {
                            System.out.println(node.getProperty(prop).toString());
                        }
                        for (String s: relTypes){
                            System.out.println(s);
                        }
                        relTypes.clear();
                    }
                }

            }   */

           /* System.out.println(counter);
            System.out.println(nodeCounter); */

            for(Relationship rel:GlobalGraphOperations.at(graphDb).getAllRelationships()){

                if(rel.getType().name().equals(RelTypes.connected7.name())){
                    relCounter++;
                    //relTypes.add(rel.getEndNode().getProperty("value").toString());
                }
            }
            /*Label myLabel = DynamicLabel.label("Person");
            for (Node node : graphDb.findNodesByLabelAndProperty(myLabel,"century",17)) {
                counter++;
            } */
            /*for (String s: relTypes){
                System.out.println(s);
            }   */
            tx.success();

            System.out.println(relCounter);

        }

    }
}
