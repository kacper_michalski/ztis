package pl.edu.agh.ztis.transformer;

import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.tooling.GlobalGraphOperations;
import pl.edu.agh.ztis.TransactionHelper;

import java.util.Iterator;

import static org.neo4j.graphdb.DynamicRelationshipType.withName;

/**
 * Created by Kacper on 2015-06-13.
 */
public class DateFilter {
    private GraphDatabaseService graphDb;

    public static void main(String[] args) {
        DateFilter typeRemover = new DateFilter();
        typeRemover.createDb();
        typeRemover.setTypeProperty();
        typeRemover.deleteTypeNodes();
        typeRemover.closeDb();
    }

    private void closeDb() {
        graphDb.shutdown();
    }

    private void createDb() {
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase("integracjaDB2");
        registerShutdownHook(graphDb);
    }

    private static void registerShutdownHook(final GraphDatabaseService graphDb) {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    public void setTypeProperty() {
        int counter = 0;
        TransactionHelper transactionHelper = new TransactionHelper(10000, graphDb);
        //try (Transaction tx = graphDb.beginTx()) {
        transactionHelper.beginTx();
        Iterator<Node> iterator = GlobalGraphOperations.at(graphDb).getAllNodes().iterator();

        while (iterator.hasNext()) {

            for (Path position : graphDb.traversalDescription()
                    .depthFirst()
                    .relationships(withName("http://xmlns.com/foaf/0.1/name"), Direction.OUTGOING)
                    .evaluator(Evaluators.fromDepth(1))
                    .evaluator(Evaluators.toDepth(1))
                    .traverse(iterator.next())) {
                Node startNode = position.startNode();
                startNode.setProperty("isPerson", "true");
                counter++;
                transactionHelper.resetTx();
            }
        }
        transactionHelper.commitTx();
        transactionHelper.closeTx();
        //tx.success();
        System.out.println(counter);
    }


    public void deleteTypeNodes() {
        try (Transaction tx = graphDb.beginTx()) {
            tx.success();
        }
    }
}
