package pl.edu.agh.ztis.importer;

import com.tinkerpop.blueprints.impls.neo4j2.Neo4j2Graph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.openrdf.model.ValueFactory;
import org.openrdf.rio.ParseErrorListener;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.ntriples.NTriplesParser;
import org.openrdf.sail.Sail;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;

import java.io.*;

public class DBpediaLoader {
    public static void main(String[] args)
            throws SailException, RDFParseException, RDFHandlerException, FileNotFoundException, IOException {
        Neo4j2Graph neo = new Neo4j2Graph(args[0]);
        Sail sail = new GraphSail(neo);
        sail.initialize();
        ValueFactory vf = sail.getValueFactory();
        SailConnection sc = sail.getConnection();
        //sc.begin();
        //CommitManager manager = TransactionalGraphHelper.createCommitManager(neo, 10000);

        String file = args[1];
        System.out.println("Loading " + file + ": ");
        loadFile(file, sail.getConnection(), sail.getValueFactory())/*, manager*/;
        System.out.print('\n');
        if (sc.isActive())
            sc.commit();
        sc.close();
        sail.shutDown();
        neo.shutdown();
    }

    private static void loadFile(final String file, SailConnection sc, ValueFactory vf/*, CommitManager manager*/) throws RDFParseException, RDFHandlerException, FileNotFoundException, IOException {
        NTriplesParser parser = new NTriplesParser(vf);
        TripleHandler handler = new TripleHandler(sc);
        parser.setRDFHandler(handler);
        parser.setStopAtFirstError(false);
        parser.setParseErrorListener(new ParseErrorListener() {

            @Override
            public void warning(String msg, int lineNo, int colNo) {
                System.err.println("warning: " + msg);
                System.err.println("file: " + file + " line: " + lineNo + " column: " + colNo);
            }

            @Override
            public void error(String msg, int lineNo, int colNo) {
                System.err.println("error: " + msg);
                System.err.println("file: " + file + " line: " + lineNo + " column: " + colNo);
            }

            @Override
            public void fatalError(String msg, int lineNo, int colNo) {
                System.err.println("fatal: " + msg);
                System.err.println("file: " + file + " line: " + lineNo + " column: " + colNo);
            }

        });
        try {
            parser.parse(new CompressorStreamFactory().createCompressorInputStream(new BufferedInputStream(new FileInputStream(new File(file)))), "http://dbpedia.org/");
        } catch (CompressorException e) {
            e.printStackTrace();
        }
    }
}
