package pl.edu.agh.ztis.extractor;

import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.Uniqueness;
import org.neo4j.tooling.GlobalGraphOperations;
import pl.edu.agh.ztis.TransactionHelper;
import pl.edu.agh.ztis.transformer.RelTypes;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import static org.neo4j.graphdb.DynamicRelationshipType.withName;

/**
 * Created by Kacper on 2015-05-25.
 */
public class GephiExporter {
    private GraphDatabaseService graphDb;

    public static void main(String[] args) {
        GephiExporter gephiExporter = new GephiExporter();
        gephiExporter.createDb(args[0]);
        gephiExporter.exportToGephi(args[1]);
        gephiExporter.closeDb();
    }

    private void createDb(String dbDir) {
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(dbDir);
        registerShutdownHook(graphDb);
    }

    private static void registerShutdownHook(final GraphDatabaseService graphDb) {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

    private void closeDb() {
        graphDb.shutdown();
    }

    public void exportToGephi(String filename) {
        try (PrintWriter writer = new PrintWriter(filename, "UTF-8")) {


            TransactionHelper transactionHelper = new TransactionHelper(10000, graphDb);
                long counter = 0;
                long extCounter = 0;
                Label myLabel = DynamicLabel.label("Person");
                //for (Node node : GlobalGraphOperations.at(graphDb).getAllNodes()) {
            transactionHelper.beginTx();
                for (Node node : graphDb.findNodesByLabelAndProperty(myLabel,"century",17)) {
                //for (Node node : GlobalGraphOperations.at(graphDb).getAllNodes()) {
                    counter++;
                    //System.out.println("wtf2");
                    //try (Transaction internalTx = graphDb.beginTx()) {
                        for (Path position : graphDb.traversalDescription()
                                .depthFirst()
                                .uniqueness(Uniqueness.NODE_GLOBAL)
                                .evaluator(Evaluators.fromDepth(1))
                                .evaluator(Evaluators.toDepth(1))
                                //.expand(new GephiExpander())
                                .relationships(RelTypes.connected8, Direction.OUTGOING)
                                //.relationships(withName("http://xmlns.com/foaf/0.1/name"), Direction.OUTGOING)
                                .traverse(node)) {
                            Node startNode = position.startNode();
                            Node endNode = position.endNode();
                            //if (startNode.getId() < endNode.getId())
                                writer.println(startNode.getProperty("value") + "\t" + endNode.getProperty("value") + "\t" + position.lastRelationship().getProperty("weight"));

                            transactionHelper.resetTx();

                        }
                    //    internalTx.success();

                    //}
                    if (counter - 100000 == 0) {
                        System.out.println(++extCounter);
                        counter = 0;
                    }

                }
            writer.flush();
            transactionHelper.commitTx();
            transactionHelper.closeTx();

                //tx.success();
            //}
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
