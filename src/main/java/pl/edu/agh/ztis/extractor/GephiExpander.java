package pl.edu.agh.ztis.extractor;

import org.neo4j.graphdb.Path;
import org.neo4j.graphdb.PathExpander;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.traversal.BranchState;
import pl.edu.agh.ztis.transformer.RelTypes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Kacper on 2015-05-25.
 */
public class GephiExpander implements PathExpander {
    private Set<String> allowedRelTypesSet = new HashSet<String>();

    public GephiExpander() {
        allowedRelTypesSet.add(RelTypes.connected3.name());
    }

    @Override
    public Iterable<Relationship> expand(Path path, BranchState branchState) {
        List<Relationship> results = new ArrayList<Relationship>();
        for (Relationship r : path.endNode().getRelationships()) {
            //System.out.println(r.getStartNode().getId()+"\t"+r.getEndNode().getId() + r.getType());
            if (allowedRelTypesSet.contains(r.getType().name())) {
                results.add(r);
                //System.out.print("added");
            }
        }
        return results;
    }

    @Override
    public PathExpander reverse() {
        return null;
    }
}
